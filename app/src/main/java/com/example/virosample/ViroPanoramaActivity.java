/*
 * Copyright (c) 2017-present, Viro, Inc.
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.virosample;

import android.app.Activity;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.virosample.orientationProvider.ImprovedOrientationSensor1Provider;
import com.viro.core.AnimationTransaction;
import com.viro.core.Box;
import com.viro.core.Camera;
import com.viro.core.ClickListener;
import com.viro.core.ClickState;
import com.viro.core.FrameListener;
import com.viro.core.Node;
import com.viro.core.Quaternion;
import com.viro.core.Scene;
import com.viro.core.Sphere;
import com.viro.core.Text;
import com.viro.core.Texture;
import com.viro.core.Vector;
import com.viro.core.ViroView;
import com.viro.core.ViroViewScene;

import java.io.IOException;
import java.io.InputStream;

import static java.lang.Math.abs;

/**
 * A sample Android activity for creating 3D scenes in a View.
 * <p>
 * Extend and override onRendererStart() to start building your 3D scenes.
 */
public class ViroPanoramaActivity extends Activity {

    private static final String TAG = ViroPanoramaActivity.class.getSimpleName();
    //protected ViroViewARCore mViroView;
    protected ViroView mViroView;
    private AssetManager mAssetManager;

    private ImprovedOrientationSensor1Provider sensor1Provider;
    private final com.example.virosample.representation.Quaternion tmpQuaternion = new com.example.virosample.representation.Quaternion();
    private Node cameraNode;
    private Camera camera;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sensor1Provider = new ImprovedOrientationSensor1Provider((SensorManager) getSystemService(SENSOR_SERVICE));


        mViroView = new ViroViewScene(this, new ViroViewScene.StartupListener() {

            @Override
            public void onSuccess() {
                createHelloWorldScene();
            }

            @Override
            public void onFailure(ViroViewScene.StartupError startupError, String s) {

            }
        });
        setContentView(mViroView);
    }


    private void createHelloWorldScene() {
        // Create a new Scene and get its root Node
        Scene scene = new Scene();

        // Load the background image into a Bitmap file from assets
        Bitmap backgroundBitmap = bitmapFromAsset("star.jpg");
        // Add a 360 Background Texture if we were able to find the Bitmap
        if (backgroundBitmap != null) {
            Texture backgroundTexture = new Texture(backgroundBitmap, Texture.Format.RGBA8, true, true);
            scene.setBackgroundTexture(backgroundTexture);
        }

        Text helloWorldText = new Text(mViroView.getViroContext(), "Hello World!", 2, 1);
        helloWorldText.setFontSize(12);
        helloWorldText.setHorizontalAlignment(Text.HorizontalAlignment.CENTER);

        final Node textNode = new Node();
        textNode.setPosition(new Vector(0, 0, -2));
        textNode.setGeometry(helloWorldText);

        textNode.setClickListener(new ClickListener() {
            @Override
            public void onClick(int i, Node node, Vector vector) {
                System.out.println("Click!");
                Toast.makeText(ViroPanoramaActivity.this, "World click", Toast.LENGTH_SHORT).show();
                camera.setPosition(new Vector(textNode.getPositionRealtime().x, textNode.getPositionRealtime().y, textNode.getPositionRealtime().z+0.5));
            }

            @Override
            public void onClickState(int i, Node node, ClickState clickState, Vector vector) {

            }
        });

        scene.getRootNode().addChildNode(textNode);

        final Node boxNode = new Node();
        boxNode.setPosition(new Vector(2, 2, -1));
        Sphere box = new Sphere(0.1f);
        //Box box = new Box(0.1f, 0.1f, 0.1f);
        boxNode.setGeometry(box);
        boxNode.setClickListener(new ClickListener() {
            @Override
            public void onClick(int i, Node node, Vector vector) {
                Toast.makeText(ViroPanoramaActivity.this, "Box click", Toast.LENGTH_SHORT).show();
                camera.setPosition(new Vector(boxNode.getPositionRealtime().x, boxNode.getPositionRealtime().y, boxNode.getPositionRealtime().z+0.5));
            }

            @Override
            public void onClickState(int i, Node node, ClickState clickState, Vector vector) {

            }
        });
        scene.getRootNode().addChildNode(boxNode);



        // Display the scene
        mViroView.setScene(scene);

        //if (true) return;

        cameraNode = new Node();
        cameraNode.setPosition(new Vector(0, 0, 0));
        //cameraNode.setRotation(new Vector((float)-Math.PI / 2.0f, 0, 0));

        camera = new Camera();
        //camera.setRotationType(Camera.RotationType.ORBIT);
        cameraNode.setCamera(camera);

        //scene.getRootNode().addChildNode(boxNode);
        mViroView.setPointOfView(cameraNode);


        mViroView.setFrameListener(new FrameListener() {
            @Override
            public void onDrawFrame() {
                System.out.println("onDrawFrame" + getQuaternion());
                camera.setRotation(getQuaternion());
            }
        });
    }

    public com.viro.core.Quaternion getQuaternion() {
        sensor1Provider.getQuaternion(tmpQuaternion);
        com.viro.core.Quaternion qq = new com.viro.core.Quaternion(tmpQuaternion.x(), tmpQuaternion.y(), tmpQuaternion.z(), tmpQuaternion.w());
        //return qq.invert().multiply(com.viro.core.Quaternion.makeIdentity().makeRotation(-90f, new Vector(1f, 0, 0)));
        return qq.invert().multiply(com.viro.core.Quaternion.makeIdentity().makeRotation(0f, new Vector(0, 0, 0)));
    }

    private void animateTextLoop(final Node textNode, Vector position) {
        AnimationTransaction.begin();
        AnimationTransaction.setAnimationDuration(5000);
        AnimationTransaction.setListener(new AnimationTransaction.Listener() {
            @Override
            public void onFinish(AnimationTransaction animationTransaction) {
                Vector position = textNode.getPositionRealtime();
                animateTextLoop(textNode, new Vector(-position.x, position.y, position.z));
            }
        });
        textNode.setPosition(position);
        AnimationTransaction.commit();
    }

    public void setCameraPosition(Node cameraNode, Vector cameraPosition, Vector lookAtPosition){
        Vector dirVector = lookAtPosition.subtract(cameraPosition);
        Vector dirVectorNorm = lookAtPosition.subtract(cameraPosition).normalize();
        Vector globalforward = new Vector(0,0,-1);
        Vector globalUp = new Vector(0,1,0);

        // Calculate the Camera's Yaw from direction vector.
        Vector dirVectorNormNoY = new Vector(dirVector.x, 0, dirVector.z);
        double theta = Math.acos(dirVectorNormNoY.normalize().dot(globalforward.normalize()));
        if (dirVectorNorm.x > 0){
            theta =  Math.toRadians(360) - theta;
        }

        // Calculate the Camera's pitch from direction vector.
        double phi = (Math.acos(dirVector.normalize().dot(globalUp.normalize())) -  Math.toRadians(90))*-1;

        // Apply rotation and position
        Quaternion quartEuler = new Quaternion((float)phi, (float)theta, 0);
        cameraNode.setRotation(quartEuler);
        cameraNode.setPosition(cameraPosition);
    }


    private Bitmap bitmapFromAsset(String assetName) {
        if (mAssetManager == null) {
            mAssetManager = getResources().getAssets();
        }

        InputStream imageStream;
        try {
            imageStream = mAssetManager.open(assetName);
        } catch (IOException exception) {
            Log.w(TAG, "Unable to find image [" + assetName + "] in assets!");
            return null;
        }
        return BitmapFactory.decodeStream(imageStream);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mViroView.onActivityStarted(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mViroView.onActivityResumed(this);
        sensor1Provider.start();
    }

    @Override
    protected void onPause() {
        sensor1Provider.stop();
        mViroView.onActivityPaused(this);
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mViroView.onActivityStopped(this);
    }
}