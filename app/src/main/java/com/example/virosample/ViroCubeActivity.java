/*
 * Copyright (c) 2017-present, Viro, Inc.
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.virosample;

import android.app.Activity;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;

import com.viro.core.Camera;
import com.viro.core.Node;
import com.viro.core.Quaternion;
import com.viro.core.Scene;
import com.viro.core.Text;
import com.viro.core.Texture;
import com.viro.core.Vector;
import com.viro.core.ViroView;
import com.viro.core.ViroViewScene;

import java.io.IOException;
import java.io.InputStream;

/**
 * A sample Android activity for creating 3D scenes in a View.
 * <p>
 * Extend and override onRendererStart() to start building your 3D scenes.
 */
public class ViroCubeActivity extends Activity {

    private static final String TAG = ViroCubeActivity.class.getSimpleName();
    //protected ViroViewARCore mViroView;
    protected ViroView mViroView;
    private AssetManager mAssetManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mViroView = new ViroViewScene(this, new ViroViewScene.StartupListener() {

            @Override
            public void onSuccess() {
                createHelloWorldScene();
            }

            @Override
            public void onFailure(ViroViewScene.StartupError startupError, String s) {

            }
        });
        setContentView(mViroView);
        mViroView.setVRModeEnabled(true);
    }

    private void createHelloWorldScene() {
        // Create a new Scene and get its root Node
        Scene scene = new Scene();

        Text helloWorldText = new Text(mViroView.getViroContext(), "Hello World!", 2, 1);
        helloWorldText.setFontSize(12);
        helloWorldText.setHorizontalAlignment(Text.HorizontalAlignment.CENTER);

        Node textNode = new Node();
        textNode.setPosition(new Vector(0, 0, -2));
        textNode.setGeometry(helloWorldText);
        scene.getRootNode().addChildNode(textNode);

        // Cub
        Bitmap top = bitmapFromAsset("sb_ocean_top.jpg");
        Bitmap bottom = bitmapFromAsset("sb_ocean_bottom.jpg");
        Bitmap left = bitmapFromAsset("sb_ocean_left.jpg");
        Bitmap right = bitmapFromAsset("sb_ocean_right.jpg");
        Bitmap back = bitmapFromAsset("sb_ocean_back.jpg");
        Bitmap front = bitmapFromAsset("sb_ocean_front.jpg");
        Texture backgroundTexture = new Texture(front, back, top, bottom, left, right, Texture.Format.RGBA8);
        scene.setBackgroundCubeTexture(backgroundTexture);

        //scene.setBackgroundCubeWithColor(Color.RED);

        /*// Create a Text geometry
        Text helloWorldText = new Text.TextBuilder().viroContext(mViroView.getViroContext()).
                textString("Hello World").
                fontFamilies("Roboto").fontSize(50).
                color(Color.WHITE).
                width(4).height(2).
                horizontalAlignment(Text.HorizontalAlignment.CENTER).
                verticalAlignment(Text.VerticalAlignment.CENTER).
                lineBreakMode(Text.LineBreakMode.NONE).
                clipMode(Text.ClipMode.CLIP_TO_BOUNDS).
                maxLines(1).build();

        // Create a Node, position it, and attach the Text geometry to it
        Node textNode = new Node();
        textNode.setPosition(new Vector(0, 0, -2));
        textNode.setGeometry(helloWorldText);
        rootNode.addChildNode(textNode);

        // Move the text around in our 3D scene
        animateTextLoop(textNode, new Vector(1,0,-2));*/

        // Display the scene
        mViroView.setScene(scene);

        //if (true) return;

        Node cameraNode = new Node();
        cameraNode.setPosition(new Vector(0, 2, 0));
        cameraNode.setRotation(new Vector((float)-Math.PI / 2.0f, 0, 0));

        final Camera camera = new Camera();
        //camera.setRotationType(Camera.RotationType.ORBIT);
        cameraNode.setCamera(camera);


        //scene.getRootNode().addChildNode(boxNode);
        mViroView.setPointOfView(cameraNode);

        // Create an TrackedPlanesController to visually display tracked planes

    }

    public void setCameraPosition(Node cameraNode, Vector cameraPosition, Vector lookAtPosition){
        Vector dirVector = lookAtPosition.subtract(cameraPosition);
        Vector dirVectorNorm = lookAtPosition.subtract(cameraPosition).normalize();
        Vector globalforward = new Vector(0,0,-1);
        Vector globalUp = new Vector(0,1,0);

        // Calculate the Camera's Yaw from direction vector.
        Vector dirVectorNormNoY = new Vector(dirVector.x, 0, dirVector.z);
        double theta = Math.acos(dirVectorNormNoY.normalize().dot(globalforward.normalize()));
        if (dirVectorNorm.x > 0){
            theta =  Math.toRadians(360) - theta;
        }

        // Calculate the Camera's pitch from direction vector.
        double phi = (Math.acos(dirVector.normalize().dot(globalUp.normalize())) -  Math.toRadians(90))*-1;

        // Apply rotation and position
        Quaternion quartEuler = new Quaternion((float)phi, (float)theta, 0);
        cameraNode.setRotation(quartEuler);
        cameraNode.setPosition(cameraPosition);
    }


    private Bitmap bitmapFromAsset(String assetName) {
        if (mAssetManager == null) {
            mAssetManager = getResources().getAssets();
        }

        InputStream imageStream;
        try {
            imageStream = mAssetManager.open(assetName);
        } catch (IOException exception) {
            Log.w(TAG, "Unable to find image [" + assetName + "] in assets!");
            return null;
        }
        return BitmapFactory.decodeStream(imageStream);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mViroView.onActivityStarted(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        mViroView.onActivityResumed(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mViroView.onActivityPaused(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mViroView.onActivityStopped(this);
    }
}