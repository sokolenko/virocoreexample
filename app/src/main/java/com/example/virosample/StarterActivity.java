package com.example.virosample;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

public class StarterActivity  extends Activity implements View.OnClickListener {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_starter);
        findViewById(R.id.btnPanorama).setOnClickListener(this);
        findViewById(R.id.btnCube).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent intent = null;
        int id = view.getId();
        if (id == R.id.btnPanorama) {
            intent = new Intent(this, ViroPanoramaActivity.class);
        } else if (id == R.id.btnCube) {
            intent = new Intent(this, ViroCubeSensorActivity.class);
        }

        if (intent!=null) {
            startActivity(intent);
        }
    }
}
